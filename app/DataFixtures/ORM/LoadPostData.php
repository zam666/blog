<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2018-03-05
 * Time: 14:07
 */

namespace App\DataFixtures\ORM;

use AppBundle\Entity\Post;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPostData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        for ($i=1; $i<=500; $i++ ) {

            $post = new Post();
            $post->setTitle($faker->sentence(3));
            $post->setLead($faker->text(250));
            $post->setContent($faker->text(500));
            $post->setCreatedAt($faker->dateTimeThisMonth);

            $manager->persist($post);
        }

        $manager->flush();
    }
}